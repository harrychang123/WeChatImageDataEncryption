# WeChatImageDataEncryption

#### 介绍
PC微信图片加密 加密

PC版微信的图片采用简单的文件二进制异或加密，异或值每台电脑都不一样，解密主要根据jpg格式（0xFF）和png格式（0x89）文件头与第一个文件的进行异或算出本机异或值，然后进行解密
![输入图片说明](https://images.gitee.com/uploads/images/2019/1231/091503_c6b09f22_1510229.png "屏幕截图.png")